﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="DWI.Clientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <p class="auto-style3">
        Clientes</p>

    <div class="auto-style5"> </div>

    <asp:GridView ID="GridView1" runat="server"  BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal"
     AutoGenerateColumns="False" OnRowCancelingEdit="GridView1_RowCancelingEdit1"  OnRowDeleting="GridView1_RowDeleting1" ShowFooter="True" OnRowUpdating="GridView1_RowUpdating1"
      OnRowCommand="GridView1_RowCommand1" OnRowEditing="GridView1_RowEditing">
       
          <Columns>
      <asp:TemplateField HeaderText="Id">
           <ItemTemplate>
              <asp:Label Text='<%# Eval("id_c") %>' runat="server"></asp:Label>
          </ItemTemplate>
      <EditItemTemplate>
           <asp:TextBox ID="txtid" Text='<%#Eval("id_c") %>' runat="server"></asp:TextBox>
      </EditItemTemplate>
      <FooterTemplate>
            <asp:TextBox runat="server" ID="txtidf"/>
      </FooterTemplate>
           </asp:TemplateField>
      <asp:TemplateField HeaderText="Nombre">
      <ItemTemplate>
          <asp:Label Text='<%# Eval("nombre") %>' runat="server"></asp:Label>
      </ItemTemplate>
      <EditItemTemplate>
          <asp:TextBox ID="txtnombre" Text='<%#Eval("nombre") %>' runat="server"></asp:TextBox>
     </EditItemTemplate>
          <FooterTemplate>
          <asp:TextBox ID="txtfn" runat="server"/>
          </FooterTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Correo">
          <ItemTemplate>
          <asp:Label Text='<%# Eval("correo") %>' runat="server"
          ></asp:Label>
          </ItemTemplate>
          <EditItemTemplate>
          <asp:TextBox ID="txtel" Text='<%#Eval("correo") %>' runat="server"></asp:TextBox>
          </EditItemTemplate>
          <FooterTemplate>
          <asp:TextBox ID="txtftel" runat="server"/>
          </FooterTemplate>
          </asp:TemplateField>
          <asp:TemplateField>
          <ItemTemplate>
          <asp:ImageButton ImageUrl="~/images/em.png" runat="server" CommandName="Edit" ToolTip="Edit" Width="20px" Height="20px"/>
          </ItemTemplate>
          <EditItemTemplate>
          <asp:ImageButton ImageUrl="~/images/delete.png" CommandName="Delete" ToolTip="Delete" Width="20px" Height="20px" runat="server"/>
          <asp:ImageButton ImageUrl="~/images/update.png" runat="server" CommandName="Update" ToolTip="Save" Width="20px" Height="20px" />
          <asp:ImageButton ImageUrl="~/images/cancelar.png" runat="server" CommandName="cancel" ToolTip="cancel" Width="20px" Height="20px" />
          </EditItemTemplate>
          <FooterTemplate>
          <asp:ImageButton ImageUrl="~/images/add.png" runat="server" CommandName="Save" ToolTip="Save" Width="20px" Height="20px" />
          </FooterTemplate>
          </asp:TemplateField>
          </Columns>

          <FooterStyle BackColor="White" ForeColor="#2DA496" />
          <HeaderStyle BackColor="#2DA496" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
          <RowStyle BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" ForeColor="#333333" />
          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White"/>

          </asp:GridView>

          <asp:Label ID="MensajeS" Text="" runat="server" ForeColor="Green"/>
          <br /> <asp:Label ID="MensajeM" Text="" runat="server" ForeColor="Red"/> 
                   
   
    
</asp:Content>
