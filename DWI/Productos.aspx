﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="DWI.Productos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
                
       .button1{
           background-color: #28D1D8; /* Green */
           border: 1px solid green;
           color: white;
           padding: 12px 26px;
           text-align: center;
           text-decoration: none;
           display: inline-block;
           font-size: 18px;
           cursor: pointer;
           float: left;
           border-style:hidden
       }
       .button1:hover {
           background-color: #28D1D8;
        }
       .button2 {
           background-color: #0099CC; /* Green */
           border: 1px solid green;
           color: white;
           padding: 10px 20px;
           text-align: center;
           text-decoration: none;
           display: inline-block;
           font-size: 14px;
           cursor: pointer;
           float: left;
           border-style:hidden
       }
       .button2:hover {
           background-color: #28D1D8;
       }


        .auto-style8 {
            width: 100%;
            height: 160px;
        }
        .auto-style10 {
            height: 48px;
        }
        .auto-style11 {
            height: 34px;
            width: 290px;
        }
        .auto-style12 {
            height: 37px;
            width: 290px;
        }
        .auto-style13 {
            height: 41px;
            width: 290px;
        }
        .auto-style15 {
            height: 34px;
            }
        .auto-style16 {
            height: 37px;
            }
        .auto-style17 {
            height: 41px;
        }


        .auto-style18 {
            width: 289px;
        }
        .auto-style19 {
            width: 261px;
        }


        .auto-style20 {
            width: 291px;
        }
        .auto-style22 {
            width: 100%;
        }
        .auto-style23 {
            width: 206px;
        }
        .auto-style24 {
            width: 291px;
            height: 98px;
        }
        .auto-style25 {
            width: 206px;
            height: 98px;
        }
        .auto-style26 {
            height: 98px;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="Button6" runat="server" Text="Alta" cssclass="button1" OnClick="Button6_Click" />
    <asp:Button ID="Button7" runat="server" Text="Edición" cssclass="button1" OnClick="Button7_Click"/>
    <asp:Button ID="Button8" runat="server" Text="Consultas" cssclass="button1" OnClick="Button8_Click"/>
   
    
    <table runat="server" id="altas" cssclass="button1" class="auto-style8">
        <tr>
            <td class="auto-style10" colspan="2">Registro de Nuevo Producto</td>
        </tr>
        <tr>
            <td class="auto-style11">Nombre</td>
            <td class="auto-style15">
                <asp:TextBox ID="TextBox1" runat="server" Width="201px"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td class="auto-style12">Precio</td>
            <td class="auto-style16">
                <asp:TextBox ID="TextBox2" runat="server" Width="198px"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td class="auto-style13">
                <br />
                cantidad en el stock</td>
            <td class="auto-style17">
                <asp:TextBox ID="TextBox3" runat="server" Width="197px"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td class="auto-style13"></td>
            <td class="auto-style17">
                <asp:Button ID="Button9" runat="server" Text="Guardar" OnClick="Button9_Click" cssclass="button2"/>
            </td>
            
        </tr>
    </table>
        
    <table style="width:100%;"  runat="server" id="edicion">
        <tr>
            <td colspan="2">Edición</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style18">Id</td>
            <td class="auto-style19">
                <asp:TextBox ID="TextBox4" runat="server" Width="197px"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="Button11" runat="server" Text="Consultar" cssclass="button2" OnClick="Button11_Click"/>
            </td>
        </tr>
        <tr>
            <td class="auto-style18">Nombre</td>
            <td class="auto-style19">
                <asp:TextBox ID="TextBox5" runat="server" Width="197px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style18">Precio</td>
            <td class="auto-style19">
                <asp:TextBox ID="TextBox6" runat="server" Width="197px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style18">cantidad</td>
            <td class="auto-style19">
                <asp:TextBox ID="TextBox7" runat="server" Width="197px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style18">&nbsp;</td>
            <td class="auto-style19">
                <asp:Button ID="Button10" runat="server" Text="Modificar" cssclass="button2" OnClick="Button10_Click"/>
            </td>
            <td>
                <asp:Button ID="Button12" runat="server" Text="Eliminar" cssclass="button2" OnClick="Button12_Click"/>
            </td>
        </tr>
    </table>
        
    <table runat="server" id="consultar" class="auto-style22">
        <tr>
            <td class="auto-style20">Por Id</td>
            <td class="auto-style23">
                <asp:TextBox ID="TextBox8" runat="server" Width="193px" OnTextChanged="TextBox8_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="Button13" runat="server" Text="Consultar"  cssclass="button2" OnClick="Button13_Click"/>
                <asp:GridView ID="GridView1" runat="server">
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="auto-style20">&nbsp;</td>
            <td class="auto-style23">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style24">Reporte General</td>
            <td class="auto-style25"></td>
            <td class="auto-style26">
                <asp:Button ID="Button14" runat="server" Text="Generar"  cssclass="button2" OnClick="Button14_Click"/>
                <asp:GridView ID="GridView2" runat="server" Height="72px">
                </asp:GridView>
            </td>
        </tr>
    </table>
        
</asp:Content>
