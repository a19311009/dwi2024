﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Ventas.aspx.cs" Inherits="DWI.Ventas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
            
   .button1{
       background-color: #28D1D8; /* Green */
       border: 1px solid green;
       color: white;
       padding: 12px 26px;
       text-align: center;
       text-decoration: none;
       display: inline-block;
       font-size: 18px;
       cursor: pointer;
       float: left;
       border-style:hidden
   }
   .button1:hover {
       background-color: #28D1D8;
    }
   .button2 {
       background-color: #0099CC; /* Green */
       border: 1px solid green;
       color: white;
       padding: 10px 20px;
       text-align: center;
       text-decoration: none;
       display: inline-block;
       font-size: 14px;
       cursor: pointer;
       float: left;
       border-style:hidden
   }
   .button2:hover {
       background-color: #28D1D8;
   }


    .auto-style8 {
        width: 100%;
        height: 160px;
    }
    .auto-style10 {
        height: 48px;
    }
    .auto-style11 {
        height: 34px;
        width: 290px;
    }
    .auto-style12 {
        height: 37px;
        width: 290px;
    }
    .auto-style13 {
        height: 41px;
        width: 290px;
    }
    .auto-style15 {
        height: 34px;
        }
    .auto-style16 {
        height: 37px;
        }
    .auto-style17 {
        height: 41px;
    }


    .auto-style18 {
        width: 289px;
    }
    .auto-style19 {
        width: 261px;
    }


    .auto-style20 {
        width: 291px;
    }
    .auto-style22 {
        width: 100%;
    }
    .auto-style23 {
        width: 206px;
    }
    .auto-style24 {
        width: 291px;
        height: 98px;
    }
    .auto-style25 {
        width: 206px;
        height: 98px;
    }
    .auto-style26 {
        height: 98px;
    }


</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td class="auto-style3">seleccionar cliente</td>
            <td class="auto-style4">
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
            </td>
            <td class="auto-style5">fecha</td>
            <td class="auto-style6">
                <asp:TextBox ID="TextBox1" runat="server" TextMode="Date"></asp:TextBox>
            </td>
            <td class="auto-style7">
                <asp:Button ID="Button6" runat="server" OnClick="Button6_Click" Text="Nueva venta" cssclass="button2"/>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Productos</td>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style6">&nbsp;</td>
            <td class="auto-style7">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton1_Click" />
            </td>
            <td class="auto-style4">
                <asp:ImageButton ID="ImageButton2" runat="server" OnClick="ImageButton2_Click" />
            </td>
            <td class="auto-style5">
                <asp:ImageButton ID="ImageButton3" runat="server" OnClick="ImageButton3_Click" />
            </td>
            <td class="auto-style6">&nbsp;</td>
            <td class="auto-style7">
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">nombre</td>
            <td class="auto-style4">
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style5">precio</td>
            <td class="auto-style6">
                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">cantidad</td>
            <td class="auto-style4">
                <asp:DropDownList ID="DropDownList2" runat="server">
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="auto-style5">
                <asp:Button ID="Button7" runat="server" OnClick="Button7_Click" Text="Subtotal" cssclass="button2" />
            </td>
            <td class="auto-style6">
                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">
                <asp:Button ID="Button8" runat="server" OnClick="Button8_Click" Text="Agregar" cssclass="button2" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                <asp:Button ID="Button9" runat="server" OnClick="Button9_Click" Text="Ordenar y pagar" cssclass="button2"/>
            </td>
            <td class="auto-style6">Total</td>
            <td class="auto-style7">
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style6">&nbsp;</td>
            <td class="auto-style7">&nbsp;</td>
        </tr>
    </table>
</asp:Content>
